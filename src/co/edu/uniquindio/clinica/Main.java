package co.edu.uniquindio.clinica;

public class Main {

    public static void main(String[] args) {
        Persona persona = new Persona();
        persona.setEmail("diego.restrepo@uniquindio.edu.co");
        persona.getAnimales().add(new Animal(TipoAnimal.PERRO, "Firulais", 10));

        System.out.println(persona);
    }
}
