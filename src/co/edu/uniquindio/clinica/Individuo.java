package co.edu.uniquindio.clinica;

public interface Individuo {
    public void setNombre(String nombre);
    public String getNombre();
}
