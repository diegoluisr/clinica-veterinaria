package co.edu.uniquindio.clinica;

import java.util.ArrayList;

public class Persona implements Individuo {

    private String email;
    private String nombre;
    private String direccion;
    private String telefono;
    private ArrayList<Animal> animales;

    public Persona() {
        this.animales = new ArrayList<>();
    }

    public Persona(String email, String direccion, String telefono) {
        this.email = email;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public ArrayList<Animal> getAnimales() {
        return animales;
    }

    public void setAnimales(ArrayList<Animal> animales) {
        this.animales = animales;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }
}
