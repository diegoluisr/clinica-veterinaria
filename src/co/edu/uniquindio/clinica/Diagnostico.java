package co.edu.uniquindio.clinica;

import java.util.Date;

public class Diagnostico {

    private Date fecha;
    private String descripcion;

    public Diagnostico() {
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
