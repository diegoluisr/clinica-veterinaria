package co.edu.uniquindio.clinica;

public class Natural extends Persona {

    private String cedula;

    public Natural() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}
