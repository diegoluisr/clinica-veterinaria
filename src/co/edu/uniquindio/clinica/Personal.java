package co.edu.uniquindio.clinica;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Personal implements Individuo {
    private String nombre;
    private String apellidos;
    private Date fechaContratacion;

    public Personal() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Date fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    public String getFechaContratacionString() {
        return new SimpleDateFormat("dd-MM-yyyy").format(fechaContratacion).toString();
    }
}
