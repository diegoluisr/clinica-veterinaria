package co.edu.uniquindio.clinica;

public class Juridica extends Persona{

    private String nit;

    public Juridica() {
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }
}
